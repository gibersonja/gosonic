package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"regexp"
	"runtime"
	"strings"
	"time"
)

func main() {

	if len(os.Args) < 2 {
		er(fmt.Errorf("no arguments given"))
	}

	t := time.Now()

	var user string
	var pass string
	var script string
	var addr_obj_script string
	var addr_grp_script string
	var serv_grp_script string
	var mgmt_addr string
	var addr_objs []string
	var addr_grps []string
	var serv_grps []string
	var commit string
	var expect string
	var scriptFile string
	var timeout string = "10"
	var sleep string = "0.2"
	var verbose string = "0"
	var test bool
	var tmp string = "/tmp"
	var log string = fmt.Sprintf("%s_goSonic.log", t.Format("2006-01-02_15.04.05"))
	var pid string
	var err error

	args := os.Args[1:]
	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" {
			fmt.Print("--user=username\tUsername\n")
			fmt.Print("--pass=password\tPassword, leave blank (i.e. --pass= ) for interactive password input\n")
			fmt.Print("--verbose\tTurn on verbose output in log file, default is disabled.\n")
			fmt.Printf("--timeout=INT\tTimeout integer in seconds, default is %s if ommited.\n", timeout)
			fmt.Printf("--sleep=INT\tTime to sleep to wait for each command to complete, default is %s if ommited.\n", sleep)
			fmt.Print("--addr-objs=...\tList of Objects: Name/IP/Mask/Zone/Host Object list is a comma seperated list while the\n")
			fmt.Print("\t\tindividual elements of each object are seperated by forward slash.  If an entry is to be ommited\n")
			fmt.Printf("\t\tjust leave the value between the slashes empty, there should always be 4 slashes for each object\n")
			fmt.Print("\t\tEXAMPLE:\t--addr-objs=mgmt-lan/1.2.3.0/255.255.255.0/VPN/,wan/4.5.6.0/255.255.255.128/WAN/4.5.6.1,msp-host////\n")
			fmt.Print("--addr-grps=...\tList of Groups: Name/obj:ab12/grp:cd34/... Group list is a comma seperated list while\n")
			fmt.Print("\t\tthe individual elements of each group are seperated by forward slash.\n")
			fmt.Print("\t\tEXAMPLE: obj:ab12 Specifies object name ab12, grp:cd34 Specifies group named cd34.\n")
			fmt.Print("--serv-grps=...\tList of Service Groups: Name/serv-obj1/serv-obj2/... Service Group list is a comma seperated\n")
			fmt.Print("\t\tlist while the individual elemnts of each group are sperated by forward slash.\n")
			fmt.Print("W.X.Y.Z\t\tManagement Address to connect to\n")
			fmt.Print("--test\t\tThis will display the expect script without executing\n")
			fmt.Print("--help\t\tPrint this help message\n")
			return
		}

		if arg == "--test" {
			test = true
		}

		if arg == "--verbose" {
			verbose = "1"
		}

		if arg == "--pass=" {
			fmt.Printf("Password: ")
			fmt.Scanln(&pass)
			fmt.Print("\033[1F\033[10C                                                  \n")
		}

		regex := regexp.MustCompile(`--user=(\S+)`)
		if regex.MatchString(arg) {
			user = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--pass=(\S+)`)
		if regex.MatchString(arg) {
			pass = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--timeout=(-?\d+)`)
		if regex.MatchString(arg) {
			timeout = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--sleep=(\d+\.?\d*)`)
		if regex.MatchString(arg) {
			sleep = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`^\d+\.\d+\.\d+\.\d+$`)
		if regex.MatchString(arg) {
			mgmt_addr = arg
		}

		regex = regexp.MustCompile(`--addr-objs=(\S+)`)
		if regex.MatchString(arg) {
			addr_objs = strings.Split(regex.FindStringSubmatch(arg)[1], ",")
		}

		regex = regexp.MustCompile(`--addr-grps=(\S+)`)
		if regex.MatchString(arg) {
			addr_grps = strings.Split(regex.FindStringSubmatch(arg)[1], ",")
		}

		regex = regexp.MustCompile(`--serv-grps=(\S+)`)
		if regex.MatchString(arg) {
			serv_grps = strings.Split(regex.FindStringSubmatch(arg)[1], ",")
		}
	}

	if user == "" {
		er(fmt.Errorf("username is not set, use: --user="))
	}

	if pass == "" {
		er(fmt.Errorf("password is not set, use: --pass="))
	}

	if mgmt_addr == "" {
		er(fmt.Errorf("management IP address not set, use: ww.xx.yy.zz"))
	}

	expect, err = exec.LookPath("expect")
	er(err)

	// Initial Expect script
	script = "exp_internal " + verbose + ";" +
		"set timeout " + timeout + ";" +
		"set username " + user + ";" +
		"set password " + pass + ";" +
		"set host " + mgmt_addr + ";" +
		"log_file " + log + ";" +
		"expect_before {;" +
		"  timeout {;" +
		"    send_user \"connection timeout to $host\\n\";" +
		"    exit 0;" +
		"  };" +
		"};" +
		"spawn ssh -q -oKexAlgorithms=+diffie-hellman-group1-sha1 -oStrictHostKeyChecking=no $username@$host;" +
		"expect \"ssword:\";" +
		"sleep 3;" +
		"sleep " + sleep + ";" +
		"send $password\\n;" +
		"expect \">\";" +
		"sleep " + sleep + ";" +
		"send \"configure\\n\";" +
		"expect -re {[#?]};" +
		"sleep " + sleep + ";" +
		"send \"yes\\n\";" +
		"expect -re {[#?]};" +
		"sleep " + sleep + ";" +
		"send \"yes\\n\";" +
		"expect \"#\";" +
		"sleep " + sleep + ";"

	// Generate Address Object portion of Expect script
	for n := 0; n < len(addr_objs); n++ {
		addr_obj := addr_objs[n]
		elements := strings.Split(addr_obj, "/")
		if len(elements) != 5 {
			er(fmt.Errorf("invalid number of slashes for object with elements: %s\nGiven %d slashes. Required 4", addr_obj, len(elements)-1))
		}
		name := elements[0]
		ip := elements[1]
		mask := elements[2]
		zone := elements[3]
		host := elements[4]

		addr_obj_script += "send \"address-object ipv4 " + name + "\\n\";" +
			"expect \"#\";" +
			"sleep " + sleep + ";"
		if ip != "" || mask != "" {
			addr_obj_script += "send \"network " + ip + " " + mask + "\\n\";" +
				"expect \"#\";" +
				"sleep " + sleep + ";"
		}
		if zone != "" {
			addr_obj_script += "send \"zone " + zone + "\\n\";" +
				"expect \"#\";" +
				"sleep " + sleep + ";"
		}
		if host != "" {
			addr_obj_script += "send \"host " + host + "\\n\";" +
				"expect \"#\";" +
				"sleep " + sleep + ";"
		}
		addr_obj_script += "send \"exit\\n\";"
	}

	// Generate Address Group portion of Expect script
	for n := 0; n < len(addr_grps); n++ {
		addr_grp := addr_grps[n]
		elements := strings.Split(addr_grp, "/")
		name := elements[0]
		addr_grp_script += "expect \"#\";" +
			"send \"address-group ipv4 " + name + "\\n\";" +
			"expect \"#\";" +
			"sleep " + sleep + ";"

		for m := 1; m < len(elements); m++ {
			addr_obj := elements[m]

			regex := regexp.MustCompile(`obj:(\S+)`)
			if regex.MatchString(addr_obj) {
				element := regex.FindStringSubmatch(addr_obj)[1]
				addr_grp_script += "send \"address-object ipv4 " + element + "\\n\";" +
					"expect \"#\";" +
					"sleep " + sleep + ";"
			}

			regex = regexp.MustCompile(`grp:(\S+)`)
			if regex.MatchString(addr_obj) {
				element := regex.FindStringSubmatch(addr_obj)[1]
				addr_grp_script += "send \"address-group ipv4 " + element + "\\n\";" +
					"expect \"#\";" +
					"sleep " + sleep + ";"
			}
		}
		addr_grp_script += "send \"exit\\n\";"
	}

	// Generate Service Group portion of Expect script
	for n := 0; n < len(serv_grps); n++ {
		serv_grp := serv_grps[n]
		elements := strings.Split(serv_grp, "/")
		name := elements[0]
		encode_space := regexp.MustCompile(`\S+%20\S+`)
		if encode_space.MatchString(name) {
			name = strings.ReplaceAll(name, "%20", " ")
			name = "\\\"" + name + "\\\""
		}
		serv_grp_script += "expect \"#\";" +
			"send \"service-group " + name + "\\n\";" +
			"expect \"#\";" +
			"sleep " + sleep + ";"

		for m := 1; m < len(elements); m++ {
			serv_obj := elements[m]
			encode_space := regexp.MustCompile(`\S+%20\S+`)
			if encode_space.MatchString(serv_obj) {
				serv_obj = strings.ReplaceAll(serv_obj, "%20", " ")
				serv_obj = "\\\"" + serv_obj + "\\\""
			}
			serv_grp_script += "send \"service-object " + serv_obj + "\\n\";" +
				"expect \"#\";" +
				"sleep " + sleep + ";"
		}
		serv_grp_script += "send \"exit\\n\";"
	}

	// Commit statment
	commit = "expect \"#\";" +
		"sleep " + sleep + ";" +
		"send \"commit\\n\";" +
		"expect \"#\";" +
		"sleep " + sleep + ";"

	script += addr_obj_script + commit + addr_grp_script + commit + serv_grp_script + commit + "send \"exit\\n\";"
	script = strings.ReplaceAll(script, ";", "\n")

	if test {
		fmt.Printf("#Expect Script:\n%s\n\n", script)
		return
	}

	pid = fmt.Sprintf("%d", os.Getpid())
	scriptFile = tmp + "/" + pid + ".tmp"
	fh, err := os.Create(scriptFile)
	er(err)
	_, err = fh.WriteString(script)
	er(err)

	cmd := exec.Command(expect, "-f", scriptFile)
	err = cmd.Run()
	er(err, scriptFile)

	fh.Close()
	err = os.Remove(scriptFile)
	er(err)

}

func er(err error, cleanup ...string) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		if len(cleanup) > 0 {
			if cleanup[0] != "" {
				_, err := os.Stat(cleanup[0])
				er(err, "")
				err = os.Remove(cleanup[0])
				er(err, "")
			}
		}
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
